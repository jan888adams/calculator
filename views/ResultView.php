<?php

require_once('calculator/StringCalculator.php');

include('CalculatorForm.php');

$result = 0.0;

if (isset($input)) {
    $calculator = new StringCalculator();
    try {
        $result = $calculator->calculateResult($input);
    } catch (Error | Exception $e){
     $result = 0.0;
    }
}

echo "<br><span class=\"fs-5 fw-bold text-secondary\">Result: </span><span class=\"fs-4\">" . $result . "</span>";