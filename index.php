<?php

require_once('View.php');

include('templates/HeadTemplate.php');
include('sections/StartSection.php');

if (isset($_GET['operation'])) {
    echo View::render('ResultView', [
        'input' => $_GET['operation'],
    ]);
} else {
    echo View::render('CalculatorForm');
}

include('sections/EndSection.php');

