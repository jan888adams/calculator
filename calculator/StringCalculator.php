<?php

require_once('math/Math.php');

class StringCalculator
{

    public function calculateResult(string $input) : float
    {

        $splitInput = explode(' ', $input);

        $inputWhitOutBrackets = $this->calculateBracketOperations($splitInput);

        $inputWhitOutFirstLevelOperations = $this->calculateFirstLevelOperations($inputWhitOutBrackets);

        return $this->calculateSecondLevelOperations($inputWhitOutFirstLevelOperations);

    }

    private function calculateBracketOperations(Array $input) : array
    {

        $inputWithoutBracketOperations = $input;

        $saveFirstBracketIndex = 0;

        for ($i = 0; $i < count($input); $i++) {

            if($input[$i] == "("){
                $saveFirstBracketIndex = $i;
            }

            elseif($input[$i] == ")"){

               $bracketSubArray = array_slice($input, $saveFirstBracketIndex, $i - $saveFirstBracketIndex);

                $inputWhitOutFirstLevelOperations = $this->calculateFirstLevelOperations($bracketSubArray);
                $result = $this->calculateSecondLevelOperations($inputWhitOutFirstLevelOperations);

                array_splice($inputWithoutBracketOperations, $saveFirstBracketIndex + 1,
                    count($bracketSubArray) - 1);

                $inputWithoutBracketOperations[] = $result;

            } else {
                $inputWithoutBracketOperations[] = $input[$i];
            }

        }

        return $inputWithoutBracketOperations;

    }

    private function calculateFirstLevelOperations(Array $input): array
    {
        $inputWhitOutFirstLevelOperations = [];

        for ($i = 0; $i < count($input); $i++) {
            $letter = $input[$i];
            if ($this->isFirstLevelOperator($letter)) {

                $result = $this->calculate(floatval($input[$i - 1]), floatval($input[$i + 1]), $input[$i]);

                $i++;

                array_pop($inputWhitOutFirstLevelOperations);
                $inputWhitOutFirstLevelOperations[] = $result;

            } else {
                $inputWhitOutFirstLevelOperations[] = $input[$i];
            }

        }

        return $inputWhitOutFirstLevelOperations;
    }

    private function calculateSecondLevelOperations(Array $input): float
    {
        $result = $input[0];

        for ($i = 0; $i < count($input); $i++) {

            if ($this->isSecondLevelOperator($input[$i])) {
                $result = $this->calculate(floatval($result), floatval($input[$i + 1]), $input[$i]);
            }
        }

        return $result;
    }

    private function calculate(float $a, float $b, string $operation) : float
    {
        $result = 0;

        if ($operation == '+') {
            $result = Math::sum($a, $b);
        } elseif ($operation == '-') {
            $result = Math::minus($a, $b);
        } elseif ($operation == '*') {
            $result = Math::multiply($a, $b);
        } elseif ($operation == '/') {
            $result = Math::division($a, $b);
        }
        return $result;
    }

    private function isSecondLevelOperator($input): bool
    {
        $operators = [];

        $operators[] = "+";
        $operators[] = "-";

        return in_array($input, $operators);
    }

    private function isFirstLevelOperator($operator): bool
    {
        $operators = [];

        $operators[] = "*";
        $operators[] = "/";

        return in_array($operator, $operators);
    }

}