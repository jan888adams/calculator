<?php


class Math
{
    static public function sum(float $a, float $b): float
    {
        return $a + $b;
    }

    static public function minus(float $a, float $b): float
    {
        return $a - $b;
    }

    static public function multiply(float $a, float $b): float
    {
        return $a * $b;
    }

    static public function division(float $a, float $b):float
    {

        return $a / $b;
    }

}